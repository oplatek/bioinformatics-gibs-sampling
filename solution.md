GIBS SAMPLING
=============

Zadání
======

V súbore MotSam.fasta máme 10 DNA sekvencií dĺžky 600 báz, v ktorých je (umelo pridaný) motív dĺžky 15 symbolov s maximálne 4 mutáciami v každom reťazci. Naprogramujte vlasntú verziu Gibbsovho vzorkovania a pokúste sa nájsť tento motív. Odovzdajte

Program - vašu implementáciu Gibbsovho vzorkovania. 
    0. Navrhnite, čo by mal program zobrazovať v priebehu výpočtu, 
    aby sme mohli sledovať vývoj kvality nájdených motívov.
    Textový popis vášho riešenia s výsledkami. (Graf)
    1. Aký motív ste našli s akými parametrami programu v sekvenciách v súbore MotSam.fasta,
    ako dlho to trvalo, čo bolo vidieť v priebehu výpočtu.
    2. Pre nájdený motív uveďte jeho skóre, tj. vzdialenosť k reťazcom DNA.
    3. Ako by sa dalo vylepšiť Gibbsovo vzorkovanie, aby bolo spoľahlivejšie, rýchlejšie a pod.
    4.Aké výsledky dáva váš program pri hľadaní motívu dĺžky 9 báz s maximálne 4 mutáciami 
    na sekvenciách v súbore MotSamShort.fasta? 
    Ako sa líšil priebeh výpočtu nad druhým zadaním od výpočtu nad prvým zadaním?

Řešení
======

Nejprve se musím přiznat, že řešení v téměř originální podobě jsem měl jíž před 3 týdny.
Kód starý 3 týdny uměl najít nejlepší řešení (konsesus) zadaní do 1h.
Bohužel nejlepší motiv M- konsensus pro lmery z DNA sekvencí-
měl jeden lmer V1 , který měl hammingovskou hamm(M, V1)=6. 
Tedy daný konsensus nesplňoval zadání.

Následující úterní cvičení mě utvrdilo, že v kódu mám bug, 
tak jsem vždy po nedělích večerech hledal, ale nenacházel. 
Nepomohlo k nalezení řešení ani navrhovaný shifting ze slajdů. 
Naštěstí jsem našel včera článek:

Detecting Subtle Sequence Signals: A Gibbs Sampling Strategy for Multiple
Alignment
Charles E. Lawrence; Stephen F. Altschul; Mark S. Boguski; Jun S. Liu; Andrew F. Neuwald;
John C. Wootton
Science, New Series, Vol. 262, No. 5131. (Oct. 8, 1993), pp. 208-214.

V tomto článku, navrhují shifting ne po jedné sekvenci, ale posunout všechny ukazatele
lmerů ve všech sekvencích najednou.
Bug je pryč a úkol 1 a úkol 2 byl řešitelný velmi rychle (v řádů desítek minut běhu programu).
Pro úkol 1 jsem ale našel stále jenom 1 řešení i pro 100 000  iterací.

Odpovědi na otázky:
    0. Během výpočtu jsem si vypisoval maximální a průměrnou vzdálenost Hammingovu vzdálenost 
       mezi 'ideálním' motivem a 'skoro výskyty'. 
       Pro přehlednost jsem ještě vypisoval číslo iterace a ideální motif a všechny 'skoro motivy'.

Ukázka 1 - 10 lmerů - 'skoro motivů' z poslední iterace úkolu 1

6802 it: gtgagagacgatgtc : 4
6802 it: gtgacacattatgtc : 0
6802 it: gtgatatgttatgac : 4
6802 it: gtgacacatggggtc : 3
6802 it: gtgacacattatgtc : 0
6802 it: gagactcataatgtc : 3
6802 it: gttagacaatatgtt : 4
6802 it: ctgaaacactatgtc : 3
6802 it: ctgacacgttatgtc : 2
6802 it: gggacacataatgtc : 2
6802 it: Found motif: gtgacacattatgtc avg-dist 2.500000, top 4

    1. Našel jsem motif gtgacacattatgtc pro úkol 1. Jak jsem zmiňoval, po implementaci shiftingu
    z článku jsem našel první řešení po 6802 iteracích (asi 4 minutách). Od předchozího řešení se 
    algoritmus nezasekl na stejné segvenci ale o malinko posunuté. 
    Kromě parametru na ukončení programu po 100 000 iteracích, který se očividně nevyužil,
    jsem nastavil shiftovaní každou 200 iteraci. 
    (Pro detaily viz test_find-motif.py:59 def ukol1())

    2. Nevím, co přesně se myslí pojmem skóre. 
    Já jsem si počítal maximalní hammingovskou vzdálenost. 
    Po prvním vyhovujícím řešení jsem algoritmus při prvním běhu zastavil.
    Zadání byl najít konsensus délky 15 a příslušné lmery, tak aby se konsensus
    lišil od každého maximálně na 4 pozicích.
    Tedy když jsem chtěl najít jen první řešení, tak jsem algoritmus zastavil
    jakmile konsensus měl hammingovskou vzdálenost nejvýše 4 pro všechny lmery.
    Průměrná hammingovská vzdálenost byla 2.5. 

    Skóre se lehce spočítá pomocí score = NUMBER_OF_DNA * LENGHT_OF_MOTIF - TotalDistance.
    TotalDistance je součet všech hammingovských vzdáleností lmerů vzhledem ke konsensu.
    (Slajd 32 BioAlg 12-5.pdf - http://dl1.cuni.cz/mod/resource/view.php?id=136333)
    Tedy je vidět, že průměrná hammingovská vzdálenost, TotalDistance a score má podobnou vypovídací hodnotu.
    Mě přijde čitelnější průměrná hammingovská vzdálenost
    
    3. Jako vylepšení jsem použil shifting naznačený ve výše zmíněné publikaci.
    Určitě by se hodilo automatické restartovaní a měnění seedu.
    Ukázalo by se jak je důležité počáteční nastavení.
    Ve zmíňěném článku také navrhují vybírat DNA_OFF ne náhodně ale v cyklu.
    (DNA_OFF je DNA řetězec, z kterého se nepočítá profil v dané iteraci.)
    Docela by mě zajímala náhodnost tohot algoritmu kdybych měl čas:).

    4. Algoritmus má daleko větší prostor pro najití náhodných řetezců.
    Taky jich daleko více našel. Přesně 65. Nevím jestli jsem při tomhle čísle
    špatně pochopil zadání. Nicméně letmým zkontrolováním náhodných vzorků,
    jsem opravdu našel konses a sadu lmerů z DNA sequencí, které se maximálně 
    liší na 4 pozicích. Na cvičení jich bylo jenom pár, je to tak?
    Pro podrobnosti viz ukol2-1.log. Konec souboru obsahuje souhrné informace.
