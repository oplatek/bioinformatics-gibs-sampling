#!/usr/bin/env python
# Author:   Ondrej Platek, Copyright 2012, code is without any warranty!
# Created:  11:23:36 21/11/2012
# Modified: 11:23:36 21/11/2012
import numpy as np
import random
from itertools import imap
import operator

random.seed(881127)
bases = ['a', 't', 'g', 'c']
ind2bases = dict(zip(bases, range(len(bases))))


def gibs(seq, l, d, numIt=1000, shiftIt=0, justFirst=True):
    '''
    Computes Gibs sampling for sequences
    in list seqs. All seqs should have the same lenght.
    Gibs sampling search for Motif M of length l.
    We are interesting of substrings (matching candidates for motif)
    of lenght l.
    '''
    randshift = 1
    s = [x.lower() for x in seq]
    n = len(s)
    m = len(s[0])
    bf = backgroundFreq(s)

    best_lmers = []

    m_l = m - l
    lmer_starts = np.array([random.randint(0, m_l) for x in range(n)])

    for i in xrange(numIt):
        dna_off = random.randint(0, n - 1)
        if shiftIt != 0 and i % shiftIt == 0:
            tmp = lmer_starts + random.randint(-randshift, randshift)
            tmp = np.vstack((np.zeros(n, dtype=np.int), tmp))
            tmp = np.max(tmp, axis=0)
            tmp = np.vstack((tmp, m_l * np.ones(n, dtype=np.int)))
            lmer_starts = np.min(tmp, axis=0)
        else:  # not shifting, computing normal gibs
            profile = build_profile(s, l, lmer_starts, dna_off)
            w_as = compute_weightsOfDnaoff(s, n, l, m_l, dna_off, bf, profile)
            choose_dnaoff_lmerstart(dna_off, lmer_starts, w_as)
        lmer, dist = debug_print(s, lmer_starts, l, i)
        if dist <= d:
            best_lmers.append((i, lmer, dist))
            if justFirst:
                break
    return best_lmers


def lmerForProfile(profile):
    # extracts the most probable lmer from profile
    lmer = np.argmax(profile, axis=0)  # indexes to bases
    return ''.join([bases[i] for i in lmer])


def compute_weightsOfDnaoff(s, n, l, m_l, dna_off, bf, profile):
    beta = 1  # Beta 3 works too.... DARK THRESHOLD MAGIC
    divNorm = n - 1 + beta
    s_off = s[dna_off]
    w_as = np.empty((m_l,))
    for i in range(m_l):
        P_profile = 1
        P_background = 1
        lmer = s_off[i:i + l]
        for k in range(l):
            x = ind2bases[lmer[k]]  # representation of k-th letter in lmer
            # TODO logarithm
            # slide 78
            P_profile *= (profile[x][k] + beta * bf[x][dna_off]) / divNorm
            P_background *= bf[x][dna_off]
        w_as[i] = P_profile / P_background
    return w_as


def choose_dnaoff_lmerstart(dna_off, lmer_starts, w_as):
    '''
    Chooses one w_a resp its index based on w_a/(all w_as) distribution
    '''
    c_w_as = np.cumsum(w_as)
    # choose random number in range [0,max(cw_sum)]
    r = random.random() * c_w_as[len(c_w_as) - 1]
    i = 0
    while c_w_as[i] < r:
        i += 1
    lmer_starts[dna_off] = i


def backgroundFreq(seqs):
    '''
    For n number of sequences in s.
    Returns n * |bases| array of frequencies
    '''
    s = np.array(seqs).view(np.chararray)
    basescounts = tuple([s.count(c) for c in bases])
    basescounts = np.reshape(np.concatenate(basescounts), (len(bases), -1))
    # number of characters in all DNA seqs except the i-th sequence
    all_counts = np.sum(basescounts, axis=1)
    all_counts.shape = (all_counts.size, 1)
    bases_nonI = all_counts - basescounts
    ms = np.sum(bases_nonI, axis=0)
    m0 = ms[0]
    assert np.all(ms == m0), 'All sequneces should have the same length'
    return bases_nonI / float(m0)


def hamm_dist(str1, str2):
    '''
    Compute hamming distance.
    '''
    return sum(imap(operator.ne, str1, str2))


def build_profile(s, l, lmer_starts, dna_off):
    p = np.zeros((len(bases), l))
    n = len(s)
    for i in range(n):
        if i != dna_off:
            for j in range(l):
                # k; bases[k] = s[lemr_starts[i] + j]
                # k is jth character of lmer for
                k = ind2bases[s[i][lmer_starts[i] + j]]
                p[k, j] += 1
    return p


def debug_print(s, lmer_starts, ml, it=-1):
    profile = build_profile(s, ml, lmer_starts, -1)  # no dna off
    motif = lmerForProfile(profile)
    motifs = [(i, x[i:i + ml]) for i, x in zip(lmer_starts, s)]
    top = total = count = 0
    for i, m in motifs:
        d = hamm_dist(motif, m)
        total += d
        if d > top:
            top = d
        count += 1
        print '%d it: %d: %s : %d' % (it, i, m, d)
    print '%d it: Found motif: %s avg-dist %f, top %d' % (it, motif, float(total) / count, top)
    return (motif, top)
