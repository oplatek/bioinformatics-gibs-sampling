#!/usr/bin/env python
# Author:   Ondrej Platek, Copyright 2012, code is without any warranty!
# Created:  11:34:24 13/11/2012
# Modified: 11:34:24 13/11/2012

import random
import gibs
random.seed(881129)
bases = ['a', 't', 'g', 'c']


####### NOT IMPORTANT SKIP TO def ukol1(): #######
def genSequence(n):
    return ''.join([bases[random.randrange(0, len(bases))] for i in xrange(n)])


def implantMotif(sek, motif, d):
    '''
    Changes list sek, replacing it part by random Motif
    where AT MOST d position is randomly changed.
    '''
    ds = range(len(motif))
    lmotif = list(motif)
    random.shuffle(ds)
    for i in range(len(motif)):
        lmotif[ds[i]] = bases[random.randrange(0, len(bases))]
    motif = ''.join(lmotif)
    return (motif, sek[:i] + motif.upper() + sek[i + len(motif):])


def test_gibs(n, sl, ml, d):
    seqs = []
    motif = genSequence(ml)
    ms = []
    for i in xrange(n):
        m, s = implantMotif(genSequence(sl), motif, d)
        ms.append(m)
        seqs.append(s)
    lmer_starts = gibs.gibs(seqs, ml)

    new_seqs = [x[:i].lower() + x[i:i + ml].upper() + x[i + ml:].lower()
                for i, x in zip(lmer_starts, seqs)]
    new_ms = [x[i:i + ml].lower() for i, x in zip(lmer_starts, seqs)]
    for s, ns, m, nm in zip(seqs, new_seqs, ms, new_ms):
        sd = gibs.hamm_dist(s, ns)
        md = gibs.hamm_dist(m, nm)
        print 'old: %s\nnew: %s\ndist seq: %d\ndist motif: %d' % (s, ns, sd, md)


def test():
    s = genSequence(50)
    motif = genSequence(10)
    motif = motif.upper()
    print motif
    s = implantMotif(s, motif)
    print s, len(s)
####### END NOT IMPORTANT ####################


def ukol1():
    motiflength = 15
    deviation = 4
    maxIt = 100000
    shiftIt = 500
    first = False  # we want all solutions
    # first = True  # we want first solution
    ukolX('MotSam.fasta', motiflength, deviation, maxIt, shiftIt, first)


def ukol2():
    motiflength = 9
    deviation = 4
    maxIt = 1000
    shiftIt = 100
    first = False  # we want all solutions
    ukolX('MotSamShort.fasta', motiflength, deviation, maxIt, shiftIt, first)


def ukolX(filename, ml, d, maxIt=1000, shiftIt=0, justFirst=True):
    with open(filename, 'rb') as f:
        lines = f.readlines()
        s = []
        for i in range(1, 20, 2):
            s.append(lines[i].rstrip().lower())
        best_motifs = gibs.gibs(s, ml, d, maxIt, shiftIt, justFirst)
        print '====== FINISHED: Solutions matching deviations ======'
        for it, motif, dist in best_motifs:
            print 'Iter: %d\tMotif: %s\tDist: %d\t' % (it, motif, dist)
        motifs = set([m for (_, m, _) in best_motifs])
        print 'All motifs matching solutions %s' % motifs
        print 'Number of Solutions %d' % len(motifs)


if __name__ == '__main__':
    ukol1()
    # ukol2()
